// Temporizador Pomodoro v1.0
// Microcontrolador: ATtiny13A 
// Creado: 12/03/2024


#include <avr/io.h>
#include <util/delay.h>
#define prescaler 64 

void start_tone(uint16_t freq, uint8_t duty)
{
   OCR0A = (F_CPU / ((uint32_t)freq * prescaler)) - 1;
   OCR0B = (OCR0A * duty) / 100;
   TCCR0B = (1<<WGM02)|(1<<CS01)|(1<<CS00);
} 
void stop_tone(void)
{
   TCCR0B = 0;
} 

volatile int divider=0;
bool isRunning = false;
int sec=00;
int min=00;
int hr=00;
int buttonPin3 = 0;
int buttonPin4 = 0;         //Estado actual del botón
int lastbuttonPin4 = 0;     // Estado previo del botón

void resetTime() {
  sec=00;
  min=00;
  hr=00;
}

void start() {
   isRunning = false;
   resetTime();                                                               
}


void setupTimer0() {
  noInterrupts();
  // Clear registers
  TCCR0A = 0;
  TCCR0B = 0;
  TCNT0 = 0;

  // 100.80645161290323 Hz (9600000/((92+1)*1024))
  OCR0A = 92;
  // CTC
  TCCR0A |= (1 << WGM01);
  // Prescaler 1024
  TCCR0B |= (1 << CS02) | (1 << CS00);
  // Output Compare Match A Interrupt Enable
  TIMSK0 |= (1 << OCIE0A);
  interrupts();
}

void setup() {
 
  setupTimer0();
  pinMode(4, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(0, OUTPUT);
  start();
}

void loop() {

buttonPin4 = digitalRead(4);
buttonPin3 = digitalRead(3);

if(buttonPin3 == LOW) { //El Pin3 se activa con LOW
     for (int i = 0; i < 5; i++){
        digitalWrite(0, HIGH);
        delay(200);
        digitalWrite(0, LOW);
        delay(200);
    }
 
 start();  //RESET
}
if (buttonPin4 != lastbuttonPin4) {
    // Si el estado del pin cambia
    if (buttonPin4 == LOW) { //El Pin4 se activa con LOW
       isRunning = !isRunning;
    }
    // Delay para evitar efecto rebote de botón 
    delay(50);
  }
   // Almacenar el estado actual como el último estado activo del botón
  lastbuttonPin4 = buttonPin4;

if(sec >= 5 && sec < 10 && min ==00 && hr ==00) {

  digitalWrite(0, HIGH);
  digitalWrite(1, HIGH);
  }else{digitalWrite(0, LOW);
  digitalWrite(1, LOW);}

if(sec >= 15 && min==00 && hr ==00) {
// mode 7, top=OCR0A, prescale=64
   TCCR0A = (1<<COM0B1)|(1<<WGM00)|(1<<WGM01);
   TCCR0B = (1<<WGM02)|(1<<CS01)|(1<<CS00);
    DDRB = 1<<DDB1; // PB1 es un otuput de audio
    PORTB = 1<<PB1;
   DDRB=255;
  
      start_tone(659,50);// 659 Hz , duty 50%
      _delay_ms(2000);
      stop_tone();
      _delay_ms(500);
      start_tone(740,10);  
      _delay_ms(2000);
      stop_tone();
      _delay_ms(500);
      start_tone(830,5); 
      _delay_ms(2000);
      stop_tone();
      _delay_ms(500);
      start_tone(880,2);
      _delay_ms(2000);
      stop_tone();
      _delay_ms(1500);
  setupTimer0();
  start();
}

ISR(TIM0_COMPA_vect) {
  
  if(divider==0 && isRunning){
  sec=sec+1;
   if(sec == 60){
    sec=00;
    min=min+1; 
    }else;
    if(min == 60){
    min=00;
    hr=hr+1;
    }
  }
  divider++;
  divider%=100;
}