# Temporizador Pomodoro v1.0

<div align="center">
<img src="img/PCB_pomodoro_1.jpg/"
     alt="render_iso"
     height="400">
</div>

Hardware compacto que te ayuda a poder gestionar el tiempo y esfuerzo realizado en las tareas diarias que desees cumplir, frente a un notebook o computador de escritorio. Transcurrido el tiempo definido para la pausa, se generan alertas definidas por un estímulo visual y uno auditivo.

La técnica pomodoro permite asistir en el cumplimiento de la tarea que te hayas propuesto, sin distracciones de ningún tipo. Para aplicarla:

1. Identifica las tareas y priorízalas.
2. Coloca un temporizador por 25 minutos, correspondiente al tiempo de trabajo. Posteriormente, descansa 5 minutos. Este ciclo corresponde a 1 pomodoro.
3. Cada 4 pomodoros descansa 20 o 30 minutos.

## Componentes
 
El componente principal es el microcontrolador ATtiny13A, el cual es empleado para programar las entradas y salidas.
Las salidas gestionadas por el microcontrolador corresponden a dos indicadores:

- 1 x indicador visual: LED rojo, el que se activará de forma intermitente transcurridos 20 minutos sugeridos para tomarse una breve pausa 5 minutos antes de continuar con el trabajo.
- 1 x indicador auditivo: buzzer pasivo, el que acompañara al indicador para que no pase desapercibido el llamado a realizar una pausa antes de continuar con las actividades.

Las entradas gestionadas por el microcontrolador, corresponden a dos botones:

- 1 x botón para dar inicio al contador
- 1 x botón para resetear el timer

Adicionalmente, el circuito considera los siguientes componentes:

- 1 x LED indicador verde
- 1 x LED indicador azul
- 6 x resistencias:
     - 2 x 470 ohm
     - 2 x 200 ohm
     - 2 x 270 ohm

- 2 x condensadores electrolíticos:
    - 1 x 0.1uF 50V
    - 1 x 10uF 50V
- 2 x diodos zener
- 10 x pin header macho: 
    - 2 x pines para poder activar la alimentación de la placa mediante la entrada USB
    - 2 x pines asociados a un jumper, para activar el modo de operación para programar el microcontrolador ATtiny13A mediante un Arduino UNO ("Arduino as ISP")
    - 2 x pines, asociados a GND, y 5V para poder alimentar la PCB mediante alimentación externa (GND y 5V provenientes de un Arduino UNO)
    - 4 x para programar el microcontrolador ATtiny13A mediante un Arduino
- 2 x jumpers corespondientes a los modos de operación señalados anteriormente

De forma complementaria, necesitaremos un programador para poder subir el código a nuestra placa de circuito. En este caso empleamos los siguientes componentes:
- 1 x Arduino UNO + cable de comunicación
- 6 x cables conectores dupont macho - hembra


## Diseño



El circuito pomodoro, fue diseñado en el software multiplataforma opensource KiCAD. El desarrollo del circuito contemplo primeramente el diseño del esquemático con sus componentes respectivos y, en seguida, el diseño y posicionamiento de los mismos en una PCB.

[Archivos Diseño PCB KiCAD](https://gitlab.com/fablab-u-de-chile/temporizador-pomodoro/-/tree/main/pcb)

Para el diseño del esquemático, se requirió importar ciertas librerías útiles que continenen simbolos y huellas (footprints) asociadas a los componentes utilizados:

[Librerias KiCAD](https://gitlab.com/fablab-u-de-chile/temporizador-pomodoro/-/tree/main/pcb/lib)

En detalle, es importante señalar que para el diseño de la PCB se ha utilizado la librerías asociada al microcontrolador ATtiny13A (MCU_Microchip_ATtiny.lib) contenida en el archivo MCU_Microchip_ATtiny.rar, y la huella asociada al conector USB (USB_A_PCB_traces_small.kicad_mod) contenida en el archivo comprimido SH_Connectors.pretty-master.rar


### Diseño de esquemático

<div align="center">
<img src="img/pomodoro_sch_1.png/"
     alt="render_iso"
     height="400">
</div>

[Archivo Esquemático KiCAD](https://gitlab.com/fablab-u-de-chile/temporizador-pomodoro/-/blob/main/pcb/Pomodoro.kicad_sch)

### Diseño de PCB

<div align="center">
<img src="img/pomodoro_pcb_1.png/"
     alt="render_iso"
     height="400">
</div>

[Archivo PCB KiCAD](https://gitlab.com/fablab-u-de-chile/temporizador-pomodoro/-/blob/main/pcb/Pomodoro.kicad_pcb)


### Preparación de Archivos

Mencionar [TUTORIAL FIREFLY]

Exportación de archivos KiCAD, uso de Inkscape y Fabmodules

## Fresado y soldadura de PCB

<div align="center">
<img src="img/PCB_pomodoro_front.png/"
     alt="render_iso"
     height="350">
<img src="img/PCB_pomodoro_back.png/"
     alt="render_iso"
     height="350">
</div>

## Programación del dispositivo Pomodoro

### Conexión Programador: "Arduino as ISP"

Programaremos el microcontrolador ATtiny13A mediante un Arduino UNO. Para ello, primeramente, se debe colocar el jumper en los pin header que activan los pines para programar el ATtiny13A. 

[IMAGEN CONEXION]

En seguida, realizar las siguientes conexiones:

Arduino______________________ATtiny13(a)

5v---------------------------Pin 8

GND------------------------Pin 4

Pin 13-----------------------Pin 7

Pin 12-----------------------Pin 6

Pin 11-----------------------Pin 5

Pin 10-----------------------Pin 1


[IMAGEN CONEXION]

En seguida, abriremos la IDE de Arduino y cargaremos un código necesario para que el Arduino UNO permita la comunicación con el PC y la programación del microcontrolador ATtiny13A. 

Para ello, ir a Archivo > Ejemplos > ArduinoISP hacer click en ArduinoISP. Cargar el código a nuestro Arduino UNO. 

<div align="center">
<img src="img/arduino_ISP_1.png/"
     alt="render_iso"
     height="400">
</div>

¡Ahora nuestro Arduino está seteado para programar otros microcontroladores!.


### Programación en Arduino - IDE

Para programar microcontroladores ATtiny mediante la IDE de Arduino, necesitaremos instalar el paquete de configuración Microcore.

Para ello, ir a Herramientas > Placas > Gestor de Placas. Luego, en el gestor, buscar e instalar Microcore. 

<div align="center">
<img src="img/arduino_microcore_1.png/"
     alt="render_iso"
     height="400">
</div>

¡Ahora podemos cargar código en el microcontrolador ATtiny13A de nuestra placa PCB!.

## Código

El siguiente código se ha empleado para evaluar el funcionamiento de los inputs, outputs y temporizador o timer interno del microcontrolador ATtiny13A.
En principio, el código permite que el circuito opere del siguiente modo:

- Primero, el dispositivo se encuentra a la espera de que se le presione el botón de inicio para comenzar el temporizador.
- Transcurridos 5 segundos, el LED rojo parpadea durante 5 segundos.
- En seguida, transcurridos 15 segundos, el buzzer actúa y se activan tonos de distintas frecuencias.

Codigo de prueba ([pomodoro.ino](https://gitlab.com/fablab-u-de-chile/temporizador-pomodoro/-/blob/main/pomodoro.ino)):

```
// Temporizador Pomodoro v1.0
// Microcontrolador: ATtiny13A 
// Creado: 12/03/2024


#include <avr/io.h>
#include <util/delay.h>
#define prescaler 64 

void start_tone(uint16_t freq, uint8_t duty)
{
   OCR0A = (F_CPU / ((uint32_t)freq * prescaler)) - 1;
   OCR0B = (OCR0A * duty) / 100;
   TCCR0B = (1<<WGM02)|(1<<CS01)|(1<<CS00);
} 
void stop_tone(void)
{
   TCCR0B = 0;
} 

volatile int divider=0;
bool isRunning = false;
int sec=00;
int min=00;
int hr=00;
int buttonPin3 = 0;
int buttonPin4 = 0;         //Estado actual del botón
int lastbuttonPin4 = 0;     // Estado previo del botón

void resetTime() {
  sec=00;
  min=00;
  hr=00;
}

void start() {
   isRunning = false;
   resetTime();                                                               
}


void setupTimer0() {
  noInterrupts();
  // Clear registers
  TCCR0A = 0;
  TCCR0B = 0;
  TCNT0 = 0;

  // 100.80645161290323 Hz (9600000/((92+1)*1024))
  OCR0A = 92;
  // CTC
  TCCR0A |= (1 << WGM01);
  // Prescaler 1024
  TCCR0B |= (1 << CS02) | (1 << CS00);
  // Output Compare Match A Interrupt Enable
  TIMSK0 |= (1 << OCIE0A);
  interrupts();
}

void setup() {
 
  setupTimer0();
  pinMode(4, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(0, OUTPUT);
  start();
}

void loop() {

buttonPin4 = digitalRead(4);
buttonPin3 = digitalRead(3);

if(buttonPin3 == LOW) { //El Pin3 se activa con LOW
     for (int i = 0; i < 5; i++){
        digitalWrite(0, HIGH);
        delay(200);
        digitalWrite(0, LOW);
        delay(200);
    }
 
 start();  //RESET
}
if (buttonPin4 != lastbuttonPin4) {
    // Si el estado del pin cambia
    if (buttonPin4 == LOW) { //El Pin4 se activa con LOW
       isRunning = !isRunning;
    }
    // Delay para evitar efecto rebote de botón 
    delay(50);
  }
   // Almacenar el estado actual como el último estado activo del botón
  lastbuttonPin4 = buttonPin4;

if(sec >= 5 && sec < 10 && min ==00 && hr ==00) {

  digitalWrite(0, HIGH);
  digitalWrite(1, HIGH);
  }else{digitalWrite(0, LOW);
  digitalWrite(1, LOW);}

if(sec >= 15 && min==00 && hr ==00) {
// mode 7, top=OCR0A, prescale=64
   TCCR0A = (1<<COM0B1)|(1<<WGM00)|(1<<WGM01);
   TCCR0B = (1<<WGM02)|(1<<CS01)|(1<<CS00);
    DDRB = 1<<DDB1; // PB1 es un otuput de audio
    PORTB = 1<<PB1;
   DDRB=255;
  
      start_tone(659,50);// 659 Hz , duty 50%
      _delay_ms(2000);
      stop_tone();
      _delay_ms(500);
      start_tone(740,10);  
      _delay_ms(2000);
      stop_tone();
      _delay_ms(500);
      start_tone(830,5); 
      _delay_ms(2000);
      stop_tone();
      _delay_ms(500);
      start_tone(880,2);
      _delay_ms(2000);
      stop_tone();
      _delay_ms(1500);
  setupTimer0();
  start();
}

ISR(TIM0_COMPA_vect) {
  
  if(divider==0 && isRunning){
  sec=sec+1;
   if(sec == 60){
    sec=00;
    min=min+1; 
    }else;
    if(min == 60){
    min=00;
    hr=hr+1;
    }
  }
  divider++;
  divider%=100;
}
```

Finalmente, si queremos que funcione de forma autónoma, colocamos el jumper en los pin header que activan la alimentación y funcionamiento del Temporizador Pomodoro mediante conexión USB.

[IMAGEN CONEXION]


## Listado de Archivos

- [Archivos KiCAD - Esquemático y PCB](https://gitlab.com/fablab-u-de-chile/temporizador-pomodoro/-/tree/main/pcb?ref_type=heads)
- [Código Arduino](https://gitlab.com/fablab-u-de-chile/temporizador-pomodoro/-/blob/main/pomodoro.ino))
- [Librerias](https://gitlab.com/fablab-u-de-chile/temporizador-pomodoro/-/tree/main/pcb/lib)
- Archivos de Corte Modela MDX-20 *.rml 


## Sugerencias Próximos Pasos

- Optimizar y limpiar el código
- Considerar un temporizador externo debido a la falta de precisión del clock interno del microcontrolador ATtiny13A
- Considerar emplear un microcontrolador (ATtiny, Atmel, etc) con mayor capacidad de procesamiento
- Fabricar el circuito en 2 capas para facilitar la soldadura de componentes
- Considerar soldar el buzzer en la capa trasera
- Considerar usar un switch de 3 posiciones, en vez de usar jumpers

